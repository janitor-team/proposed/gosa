#!/bin/bash

classes=$(grep -r -E "^class [_a-zA-Z0-9]" * | sed -r -e 's/(.*):class\ ([^\ {]+).*$/\2:\1/' | sort | uniq)

for line in $classes; do
  split=(${line//:/ })
  class=${split[0]}

  if [ "${last_class}" = "${class}" ]; then
    continue
  fi
  last_class="${class}"
  echo $class   ----   ${split[1]}

  sed -i "s/function ${class} *(/function __construct(/" ${split[1]}

  find * -type f | egrep "\.php|\.inc" | while read file; do
    if [ "${class}" != "xml" ]; then
	sed -r -e "s/${class}::${class}/${class}::__construct/" -i "$file"
    fi
  done

done
